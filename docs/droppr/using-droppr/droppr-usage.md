---
sidebar_position: 2
title: Droppr Usage
description: How to use Droppr
---

# Droppr Usage

## Install Command

The Droppr install command takes the form of 
```
droppr install [flags]

Flags:
  -b, --bundle string    Hoppr bundle to install (required)
  -h, --help             help for install
  -l, --logfile string   Log file location (default "droppr.log")
      --config string    config file (default is .droppr.yml or $HOME/.config/.droppr.yml)
      --num_workers int  set number of worker threads for processing (default 10)
```
If the `config` file is not specified, the program first looks for a `.droppr.yml` file in the current working directory.  If not found there, it looks in `$HOME/.config/.droppr.yml`.

If `num_workers` (the number of worker threads) is not specified on the command line, the value may be specified in the `config` file.  If not found in either of those locations, a default value of 10 is used.

For example:
```
droppr install --bundle bundle.tar.gz --logfile droppr_log.txt --config droppr_config.yml
```

## Configuration File Schema

#### A sample config file:
```
num_workers: 20
unpack_directory: /tmp

repos:
    - purl_type: maven
      target_type: nexus
      target_location: http://127.0.0.1:8081/repository/droppr_demo
      username: admin
      password_env: NEXUS_PW
      nexus:
         api_url:   http://127.0.0.1:8081
         repo_name: droppr_demo
        
      
    - purl_type: generic
      target_type: filesys
      file_system:
          target_directory: target/directory

    - purl_type: pypi
      regex_match: "pypi.org"
      target_type: local
      local_install:
          package_manager_command: ["python3", "-m", "pip"]

    - purl_type: pypi
      target_type: filesys
      file_system:
          target_directory: my/pypi/whls
```
### Configuration Field Descriptions
The `num_workers` field sets the number of worker threads to be used by Droppr.  This value can be overridden by the `--num_workers` command line option, and will default to 10 if omitted.

The `unpack_directory` specifies where the bundle will be temporarily extracted on disk. Optional, and defaults to the OS temp directory (typically `/tmp` on Linux).

The `repos` field is an array, with one entry per purl type.  Each entry indicates how artifacts of that purl type are to be handled.  The fields within each repo are:
- `purl_type`: The purl type being handled by this repo.  Required

- `regex_match`: A regular expression to check against the _source_ repository (the repository from which the artifact was originally copied) to use this repository.  If omitted, it defaults to an empty string (which will always match).  Note that the repository selected will always be the _first_ successful match for a purl type, if there are multiple options.  Therefore, the default (`regex_match` missing or "") should come _last_.
   -  `regex_match` pattern matches can be referenced inside the following configuration fields:
      - `target_directory` fields (in both `local_install:` and `file_system:` target configurations - see below)
      - `repo_name` field in the `nexus:` target configuration  

- `target_type`: Indicates how artifacts of this purl type are to be handled.   Options are:
	- `filesys`, `filesystem`, `file_sys`, or `file_system`: Copy artifacts to a local directory
      - Must be accompanied with a [file system target configuration](#file-system-target-configuration) (see below)
	- `nexus`: Install artifacts to a Nexus repository
      - Must be accompanied with a [Nexus target configuration](#nexus-target-configuration) (see below)
	- `local`, `localinstall`, or `local_install`: Install the artifacts directly onto the local system
      - May be accompanied with a [local install target configuration](#local-install-target-configuration) (see below)

- `username`: User to be used to access Nexus.  Required for `nexus`, ignored otherwise.

- `password_env`: Environment variable containing the password to be used to access Nexus.  Required for `nexus`, ignored otherwise.

#### Target Type Configurations
- Depending on the desired target type, one of the following configuration structures (`file_system:`, `local_install:` , or `nexus:`) is used in each repo entry to indicate how and where the artifacts are to be distributed.  No more than 1 should be specified per repo entry.  
##### File System Target Configuration
```
file_system:
    target_directory: {destinationDirectory}
```
  - `file_system`: indicates where to copy artifact(s) to a file system. (Required when `target_type` is set to `filesys`)
     -  `target_directory` Indicates the destination directory to write the artifact(s). (Required)
         - Can reference pattern matches ( e.g. `$1`, `$2`, etc...) from the `regex_match` if desired.
##### Local Install Target Configuration
```
local_install:
     package_manager_command: [{cmd},{arg0},...,{argn}]
     target_directory: {destinationDirectoryGitInstall}

```

  - `local_install`: Indicates to install the artifact(s) directly to the local system. (May be used if `target_type` specifies `local`)

    - `package_manager_command`: Command (as an array of strings) used to access the appropriate package manager.  Optional for `local` installs, ignored otherwise. 
       - If omitted, a reasonable default command is used (_e.g._ `pip` for pypi artifiacts).  If the pypi installation requires sudo access, and uses pip3 rather than pip, the value would be `["sudo", "pip3"]`.
    - `target_directory`: Indicates the target directory for PURL type 'git' installs. Otherwise ignored for all other PURL types.
       - Can reference pattern matches ( e.g. `$1`, `$2`, etc...) from the `regex_match` if desired.

##### Nexus Target Configuration
```
nexus:
    api_url: {NexusInstanceURL}
    repo_name: {NexusRepositoryName}
    docker_port: {portNumber}
    docker_protocol: {http|https}
    docker_url: {URLforDockerInteractions}
    apt_signing_keypair_file: {AptSigningFileName}
```
  - `nexus`:  _Nexus installs only._ Indicates where to install artifact(s) to a Nexus repository. (Required when `target_type` is set to `nexus`)
    - `api_url`: URL of the Nexus instance (e.g. `https://my-nexus.com:8081`)
    - `repo_name`: Repository name within a Nexus instance. 
       - e.g. If your repository is located at `https://my-nexus.com:8081/repository/my-repo`, then `repo_name` should be set to `my-repo`
       - If the reposiory is not specified, a repository name will be generated based on the purl type (_e.g._ `droppr_pypi`)
        - Can reference pattern matches ( e.g. `$1`, `$2`, etc...) from the `regex_match` if desired.
    - `docker_port`: Specifies the port on the Nexus server for Docker processing.
        - Defaults to `5000`
        - Ignored if `docker_url` is specified.
    - `docker_protocol`: Specifies the docker transfer protocol (`http` or `https`). 
        - If not specified, defaults to the protocol specified in the `api_url`.  
        - Ignored if `docker_url` is specified
    - `docker_url`: This is the URL to be used for docker interactions with the Nexus repository.  
        - If specified, `docker_port` and `docker_protocol` are ignored, and the Nexus docker repository will be assumed valid and will not be created.
    - `apt_signing_keypair_file`: _APT PURLs only._ Specifies the GPG signature file for APT PURLs.  Otherwise ignored.

