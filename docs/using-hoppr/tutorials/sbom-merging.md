---
sidebar_position: 5
---

# SBOM Merging Overview

Elements of two or more CycloneDX SBOMs can be merged using the `hopctl merge` subcommand.
This subcommand dynamically updates Hoppr's CycloneDX models with a custom hash method.
This custom hash method uses select model types to define equality rules, and unique mapping/lookup
is added for each model type. This capability performs a "deep merge" of components and their metadata
providing a comprehensive merged SBOM. It can also be used to follow external references of type `bom`
and expand those references into a single bom. This includes flattening nested components, and properly
modeling them with `depends_on`.

The intent of hopctl merge is to dynamically update hoppr-cyclonedx-models with a custom `__hash__` method.

- Creates CycloneDXBaseModel class as base class for all hoppr-cyclonedx-models
- Custom `__hash__` method for select model types to define rules for equality
- Adds unique ID mapping/lookup for each model type
- Adds `find` method to return a previously loaded object of the subclass model type
- Adds `merge` method with generic logic on all CycloneDX models to merge an object of the same type into itself

## Hopctl Merge subcommands

Hopctl Merge offers several customization options.

```console
Usage: hopctl merge [OPTIONS]

Merge all properties of two or more SBOM files

| Options       | Flag          | Type          | Description                                                                |
| ------------- | ------------- | ------------- | -------------------------------------------------------------------------- |
|  --manifest   | -m            | FILE          | Manifest file containing SBOMs to merge                                    |
| --sbom        | -s            | FILE          | SBOM file to merge (can be specified multiple times)                       |
| --sbom-dir    | -d            | DIRECTORY     | Directory containing SBOM files to merge (can be specified multiple times) |
| --sbom-url    | -u            | URL           | URL of SBOM to merge (can be specified multiple times)                     |
| --output-file | -o            | FILE          | Path to output file [default: hopctl-merge-YYMMDD-HHMMSS.json]             |
| --deep-merge  |               |               | Resolve and expand externalReferences in-place                             |
| --flatten     |               |               | Flatten nested components into single unified list                         |
| --help        | -h            |               | Show this message and exit                                                 |
```

## Examples

```shell
# Merges example and example 2
hopctl merge --sbom sbom.example.json --sbom sbom.example2.json

# Merges all sboms in the sboms directory
hopctl merge --sbom-dir ../sboms

# Applies deep merge and directs it to an outfile named hopctl-merge-example.json
hopctl merge --sbom sbom.example.json --sbom sbom.example2.json --deep-merge --output-file ../merged-sboms/hopctl-merge-example.json
```
