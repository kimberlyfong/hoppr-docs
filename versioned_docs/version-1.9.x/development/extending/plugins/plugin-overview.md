---
sidebar_position: 1
title: Plugin Architecture 101
---

Hoppr's core architecture utilizes plugins to perform tasks.  Plugins are specified (and optionally configured) in the [`transfer`](/docs/using-hoppr/input-files/transfer.md) configuration file.  All plugins should derive from the [Hoppr plugin base class](https://gitlab.com/hoppr/hoppr/-/blob/dev/hoppr/base_plugins/hoppr.py#L2), which is described below:

:::tip

This plugin architecture overview builds on first principles discussed in [Hoppr Processing 101](../../../using-hoppr/tutorials/processing-101.md).

:::

## Plugin Interface Functions

Each [Hoppr plugin](https://gitlab.com/hoppr/hoppr/-/blob/dev/hoppr/base_plugins/hoppr.py#L2) runs one or more of the following methods, in order:

- `pre_stage_process` is run before any SBOM component processing.  Initialization should be performed in this method.
- `process_component` is run on each component of the consolidated SBOM.
- `post_stage_process` is run after all SBOM components have been processed.  Cleanup tasks should be performed here.

## Plugin Variables

- `required_commands` is a list of system dependencies (e.g. `mvn`, `git`) required for a plugin to execute
- `supported_purl_types` is a list of PURLs a plugin can process.  An empty list means a plugin should receive `ALL` SBOM components in `process_component`
- `default_component_coverage` indicates how often each SBOM component should be processed.  Defaults to `OPTIONAL`
- `bom_access` indicates if a plugin should be allowed to change the underlying SBOMs.  Default is `NO_ACCESS`, and we recommend
always using the principle of least access


## Plugin Types

Plugins are grouped into categories based on their intended purpose.  In most cases, these categories are simply convenient ways to refer to similar plugins, and is not actually reflected in the code.

- [**Collectors**](./collector.md) pull artifacts from various repositories and store them for later inclusion in bundles
- **Processors** modify the SBOM or its components
    - **Reports** _(coming soon)_ provide summary information about a portion of a Hoppr run
    - **Filters** _(coming soon)_ add or remove components in the delivered SBOM
- **Bundlers** take the artifacts copied by the collectors and package them in a bundle of some kind, suitable for transfer